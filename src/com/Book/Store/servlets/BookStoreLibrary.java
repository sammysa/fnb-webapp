/**
 * @author Samuel Phatlane
 *
 */

package com.Book.Store.servlets;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class BookStoreLibrary{
	
	public static void main(String[] args) throws Exception{
		BookStoreLibrary bookStoreLibrary = new  BookStoreLibrary();
		InputStream bk = bookStoreLibrary.getClass().getResourceAsStream("/JavaDevelopersAssessment.xml");
		BufferedReader fileIn = new BufferedReader(new InputStreamReader(bk));
		
		String line;
		while((line = fileIn.readLine()) != null)
		{
			System.out.println(line);
		}
	}
}

