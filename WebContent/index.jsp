<%@ taglib prefix = "core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "xmlFile" uri="http://java.sun.com/jsp/jstl/xml" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<% 	String bookTitle = ""; 
	String bookCategory = "";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Read XML Document</title>

<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
Hello Sammy
<h1>Display Book Details</h1>
<br>
	<core:import var = "bookInfo" url="/bookstore.xml" />
	
	<xmlFile:parse xml = "${bookInfo}" var = "output"/>
	<table>
		<tr>
			<th>Title</th>
		    <th>Category</th>
		    <th>Year</th>
		    <th>Price</th>
		    <th>Actions</th>
		</tr>
		<%
			bookTitle = "";
		%>
		<tr>	
			<td><xmlFile:out select="$output/bookstore/book[1]/title"/></td>
			<td>Category</td>
			<!-- <td><xmlFile:out select="$output/bookstore/book[1]/category"/></td> -->
			<td><xmlFile:out select="$output/bookstore/book[1]/year"/></td>
			<td><xmlFile:out select="$output/bookstore/book[1]/price"/></td>
			<td>Edit | Delete</td>
		</tr>
		<tr>	
			<td><xmlFile:out select="$output/bookstore/book[2]/title"/></td>
			<td>Category</td>
			<!-- <td><xmlFile:out select="$output/bookstore/book[1]/category"/></td> -->
			<td><xmlFile:out select="$output/bookstore/book[2]/year"/></td>
			<td><xmlFile:out select="$output/bookstore/book[2]/price"/></td>
			<td>Edit | Delete</td>
		</tr>
		<tr>	
			<td><xmlFile:out select="$output/bookstore/book[3]/title"/></td>
			<td>Category</td>
			<!-- <td><xmlFile:out select="$output/bookstore/book[1]/category"/></td> -->
			<td><xmlFile:out select="$output/bookstore/book[3]/year"/></td>
			<td><xmlFile:out select="$output/bookstore/book[3]/price"/></td>
			<td>Edit | Delete</td>
		</tr>
	
	</table>
	<!-- 
	<b>Book Title</b>:
	<xmlFile:out select="$output/bookstore/book[1]/title"/>
	<br>
	 -->
</body>
</html>