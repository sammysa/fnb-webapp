<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "java.io.*,java.util.*,java.sql.*"%>
<%@ page import = "javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix = "c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix = "sql"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Data From Database</title>


<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>

<sql:setDataSource var = "snapshot" driver = "com.mysql.jdbc.Driver"
         url = "jdbc:mysql://196.46.186.134/prologiq_jdbc_test"
         user = "prologiq_jadmin"  password = "@PrologiQ29"/>
         


<sql:query dataSource = "${snapshot}" var = "result">
	SELECT * from book;
</sql:query>

<h1>Details Read From The Database</h1>
<table>
  <tr>
    <th>Book Title</th>
    <th>Category</th>
    <th>Year</th>
    <th>Price</th>
  </tr>
  <tr>
    <td>*******************</td>
    <td>##########</td>
    <td>0000</td>
    <td>R 00.00</td>
  </tr>
  <c:forEach var = "row" items = "${result.rows}">
	  <tr>
	    <td><c:out value = "${row.tittle}"/></td>
	    <td><c:out value = "${row.tittle}"/></td>
	    <td><c:out value = "${row.year}"/></td>
	    <td>R <c:out value = "${row.price}"/></td>
	  </tr>
  </c:forEach>

</table>
</body>
</html>